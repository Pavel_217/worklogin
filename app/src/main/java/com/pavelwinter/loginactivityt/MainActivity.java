package com.pavelwinter.loginactivityt;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class MainActivity extends AppCompatActivity {

    EditText mEditText;
    Button mButton;
    String number;
    Observable<Long> numberObservable;
    Observer mObserver;

    private int mPreviousLength;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEditText = (EditText) findViewById(R.id.et2);
        mButton = (Button) findViewById(R.id.btn);

        mButton.setEnabled(false);
        mEditText.requestFocus();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);


        mEditText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                // сначала определяем, что это не удаление
                if (mEditText.length() < mPreviousLength) {
                } else {

                    if (s.length() == 1) {
                        mEditText.setText("");
                        mEditText.setText("(" + s);
                    } else if (s.length() == 4) {
                        mEditText.setText(s + ")");
                    } else if (s.length() == 8) {
                        mEditText.setText(s + "-");
                    } else if (s.length() == 11) {
                        mEditText.setText(s + "-");
                    } else if (s.length() == 14) {    // цифр 10 + () + -- итого 14 символов

                        number = s.toString();
                        mEditText.setEnabled(false);
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                        mButton.setEnabled(true);

                        String parsedNumber = "8";

                        for (int i = 0; i < number.length(); i++) {

                            Character c = number.charAt(i);
                            if (Character.isDigit(c)) {
                                parsedNumber += c;
                            }
                        }

                        Long numberLong = Long.valueOf(parsedNumber);

                        numberObservable = Observable.just(numberLong);

                    }
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                mPreviousLength = s.length();
                mEditText.setSelection(mEditText.getText().length());
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                mEditText.setSelection(mEditText.getText().length());

                }
            });
        }





            public void ocl(View v) {

                mObserver = new Observer<Long>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Long value) {
                        Toast.makeText(getApplicationContext(), "Bаш номер: " + value, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                };

                numberObservable.subscribe(mObserver);
            }
        }